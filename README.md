# Mental Health Repository

My current Linux installation is incredibly lucky, so I needed a place to store my config files and scripts.
I will never explain how to use them, but maybe someone might find them useful (after all, I _do_ make use of a [a similar repository](https://gitlab.com/kappanneo/home)).
You may be interested if:

- You run __Manjaro__
- You use the __i3__ window manager
- You use __zsh__ (if you don't, it's just because you are not aware that it exists!)
- You use __redshift__
- You don't dual boot and don't care about GRUB stuff